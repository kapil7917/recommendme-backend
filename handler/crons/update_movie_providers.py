
from handler.base_request_handler import BaseRequestHandler
from lib import http_response, constants
from lib.task_queue import TaskQueue
from clients.just_watch_client import JustWatchClient
from datastore import Movie, JustWatchMovie

import logging
import json


class UpdateMovieProvidersHandler(BaseRequestHandler):

    def _post(self):
        # Get movies
        query = Movie.query()
        movies, cursor, more = query.fetch_page(page_size=100)
        while more:
            for movie in movies:
                logging.debug("Updating providers for movie: %s", movie.key)

                try:
                    justwatch_movie = JustWatchMovie.query(JustWatchMovie.imdb_id == movie.imdb_id).get()
                    if justwatch_movie:
                        movie_providers = JustWatchClient().get_available_providers_for_a_title_in_country(
                            title=justwatch_movie.name,
                            release_year=int(justwatch_movie.year),
                            country_code="IN")

                        justwatch_movie.available_at = movie_providers
                        justwatch_movie.available_at_list = list(movie_providers.keys())
                    else:
                        movie_providers = JustWatchClient().get_available_providers_for_a_title_in_country(
                            title=movie.name,
                            release_year=int(movie.year),
                            country_code="IN")
                        justwatch_movie = JustWatchMovie()
                        justwatch_movie.imdb_id = movie.imdb_id
                        justwatch_movie.name = movie.name
                        justwatch_movie.year = movie.year
                        justwatch_movie.available_at = movie_providers
                        justwatch_movie.available_at_list = list(movie_providers.keys())

                    justwatch_movie.put()

                    movie.available_at = movie_providers
                    movie.available_at_list = list(movie_providers.keys())
                    movie.put()
                except Exception:
                    logging.error("Failed to get providers for movie: %s", movie.key)

            movies, cursor, more = query.fetch_page(page_size=40, start_cursor=cursor)

        http_response.set_200_response(self.response, {"message": "Added providers for movies"})


class UpdateMovieProvidersCronHandler(BaseRequestHandler):

    def _get(self):
        logging.info("Started cron job to update movie providers")
        task_queue = TaskQueue(queue="test-queue")
        task_queue.add_task(payload=json.dumps({}),
                            task_url="/update/movie/providers")

        http_response.set_200_response(self.response, "Started cron to update movie providers")
