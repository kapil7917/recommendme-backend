
from datastore import MovieProfile
from lib import constants


class MovieProfileClass(object):

    @staticmethod
    def update_movie_profile(user, genres, languages, years, interest_rating, streaming_platforms=[], send_surprise_reco=True):
        movie_profile = MovieProfileClass.get_movie_profile(user)
        if not movie_profile:
            movie_profile = MovieProfile(user=user)

        if genres and isinstance(genres, list):
            movie_profile.genres = genres
        if languages and isinstance(languages, list):
            movie_profile.languages = languages
        if years and isinstance(years, list):
            movie_profile.years = years
        if streaming_platforms is not None and isinstance(streaming_platforms, list):
            movie_profile.streaming_platforms = streaming_platforms
        if interest_rating and isinstance(interest_rating, int):
            movie_profile.interest_rating = interest_rating
        movie_profile.send_surprise_reco = send_surprise_reco

        movie_profile.put()

    @staticmethod
    def get_movie_profile(user):
        return MovieProfile.query(MovieProfile.user == user).get()

    @staticmethod
    def get_movie_profile_json(user):
        movie_profile = MovieProfileClass.get_movie_profile(user)

        genre_list = []
        language_list = []
        years_list = []
        streaming_platforms = []
        send_surprise_reco = True
        rating = 0

        if movie_profile:
            rating = movie_profile.interest_rating

        for genre in constants.SUPPORTED_GENRES:
            genre_item = {
                'property': genre,
                'display_value': constants.GENRES_PROPERTY_TO_DISPLAY_MAP.get(genre)
            }
            if movie_profile and genre in movie_profile.genres:
                genre_item['selected'] = True
            else:
                genre_item['selected'] = False
            genre_list.append(genre_item)

        for language in constants.SUPPORTED_LANGUAGES:
            language_item = {
                'property': language,
                'display_value': constants.LANGUAGES_PROPERTY_TO_DISPLAY_MAP.get(language)
            }
            if movie_profile and language in movie_profile.languages:
                language_item['selected'] = True
            else:
                language_item['selected'] = False
            language_list.append(language_item)

        for year in constants.SUPPORTED_YEARS:
            year_item = {
                'property': year,
                'display_value': constants.YEARS_PROPERTY_TO_DISPLAY_MAP.get(year)
            }
            if movie_profile and year in movie_profile.years:
                year_item['selected'] = True
            else:
                year_item['selected'] = False
            years_list.append(year_item)

        if movie_profile and movie_profile.streaming_platforms is not None and isinstance(movie_profile.streaming_platforms, list) \
                and len(movie_profile.streaming_platforms) > 0:
            streaming_platforms = movie_profile.streaming_platforms

        if movie_profile and movie_profile.send_surprise_reco is not None:
            send_surprise_reco = movie_profile.send_surprise_reco

        movie_profile_json = {
            'rating': rating,
            'genres': genre_list,
            'languages': language_list,
            'years': years_list,
            'streaming_platforms': streaming_platforms,
            'send_surprise_reco': send_surprise_reco
        }

        return movie_profile_json
