from justwatch import JustWatch
from clients.base_client import BaseClient


class JustWatchClient(BaseClient):

    def __init__(self):
        super(BaseClient, self).__init__()

    def get_available_providers_in_country(self, country_code):
        just_watch = JustWatch(country=country_code)
        provider_details = just_watch.get_providers()
        return provider_details

    def get_available_providers_for_a_title_in_country(self, title, release_year, country_code):
        just_watch = JustWatch(country=country_code)
        result = just_watch.search_for_item(query=title,
                                            page_size=1,
                                            release_year_from=release_year,
                                            release_year_until=release_year)
        providers = {}

        for item in result['items']:
            item_title = item['title']
            item_release_year = item['original_release_year']
            if item_title.lower() == title.lower() and item_release_year == release_year and 'offers' in item:
                offers = item['offers']
                for offer in offers:
                    if offer['monetization_type'] in ["flatrate", "rent"]:
                        urls = offer['urls']
                        web_url = urls['standard_web']

                        if "netflix.com" in web_url:
                            providers['Netflix'] = web_url
                        elif "primevideo.com" in web_url:
                            providers['Prime Video'] = web_url
                        elif "hotstar.com" in web_url:
                            providers['Hotstar'] = web_url
                        elif "mubi.com" in web_url:
                            providers['Mubi'] = web_url
                        elif "zee5.com" in web_url:
                            providers['Zee5'] = web_url
                        elif "youtube.com" in web_url:
                            providers['Youtube Movies'] = web_url
                        elif "play.google.com" in web_url:
                            providers['Google Play'] = web_url
                break
        return providers
