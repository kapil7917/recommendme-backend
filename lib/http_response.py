import json


def set_headers(headers):
    headers["Content-Type"] = "application/json"


def set_200_response(response, json_data):
    set_headers(response.headers)
    response.status_int = 200
    response.out.write(json.dumps(json_data))


def set_200_response_2(response, data):
    set_headers(response.headers)
    response.status_int = 200
    response.out.write(data)


def set_400_response(response, error_message):
    set_headers(response.headers)
    response.status_int = 400
    error_response = {}
    error_response['message'] = error_message
    response.out.write(json.dumps(error_response))


def set_410_response(response, error_message):
    set_headers(response.headers)
    response.status_int = 410
    error_response = {}
    error_response['message'] = error_message
    response.out.write(json.dumps(error_response))
