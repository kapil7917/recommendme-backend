from datastore import Recommendation
from lib.recommendation_action_events import RecommendationActionEventsClass
from lib.user_movie_recommendation import UserMovieRecommendationClass
from lib import constants
import logging
from clients.facebook_client import FacebookClient
from google.cloud import ndb


class RecommendationClass(object):
    CATEGORY_MOVIE = "movie"

    def __init__(self, user=None, creator=None):
        self._user = user
        self._creator = creator

    def get_recommendation(self, recommendation_id, recommendation_type):
        if recommendation_type == self.CATEGORY_MOVIE:
            return UserMovieRecommendationClass().get_by_id(recommendation_id)

    def get_recommendations_for_user(self, user, access_token, is_bookmarked=False, category="movie", cursor=None):
        recommendations_list = []
        # Getting movie recommendations only for now

        if category == "movie":
            recommendations, next_curs, more = UserMovieRecommendationClass().\
                get_user_movie_recommendations(user_key=user.key,
                                               is_bookmarked=is_bookmarked,
                                               is_recommended=False,
                                               is_not_recommended=False,
                                               cursor=cursor)
            for recommendation in recommendations:
                movie_recommendation = UserMovieRecommendationClass().generate_user_movie_recommendation(recommendation)
                # if movie_recommendation:
                #     movie_recommendation['recommendation_users_info'] = self._get_recommendation_friends_info(movie_key=recommendation.movie,
                #                                                                                               user=user,
                #                                                                                               access_token=access_token)
                recommendations_list.append(movie_recommendation)

        return recommendations_list, next_curs, more

    def _get_recommendation_friends_info(self, movie_key, user, access_token):
        recommendations = UserMovieRecommendationClass().get_movie_recommendations(movie_key)

        total_recommendations = len(recommendations)
        fb_friends_info = FacebookClient().get_user_friends_details(access_token)
        fb_friends_ids = fb_friends_info.get('ids')

        friends_names = []
        picture_url = None

        if len(fb_friends_ids) > 0:
            for recommendation in recommendations:
                recommendation_user = recommendation.user.get()
                if recommendation_user.facebook_userid and recommendation_user.facebook_userid in fb_friends_ids:
                    logging.debug("User with fb id %s is in friend list of %s and recommended %s", recommendation_user.facebook_userid, user.key, recommendation.key)
                    friends_names.append(recommendation_user.first_name)
                    if not picture_url:
                        picture_url = self._get_facebook_profile_pic_url(recommendation_user.facebook_userid)
                    if len(friends_names) > 2:
                        break
        else:
            logging.debug("No friends of user %s has installed app", user.key)

        if not picture_url and total_recommendations > 0:
            logging.debug("No friends recommended so putting picture url of first recommender")
            picture_url = self._get_facebook_profile_pic_url(recommendations[0].user.get().facebook_userid)

        return {
            'total_recommendations_count': total_recommendations,
            'friends_names': friends_names,
            'picture_url': picture_url
        }

    def _get_facebook_profile_pic_url(self, id):
        if id:
            return "http://graph.facebook.com/" + id + "/picture?type=square"

