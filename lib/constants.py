
ACTION_RECOMMENDATION_BOOKMARK = 'bookmark'
ACTION_RECOMMENDATION_REMOVE = 'remove'
ACTION_RECOMMENDATION_RECOMMEND = 'recommend'
ACTION_RECOMMENDATION_NOT_RECOMMEND = 'not_recommend'

SUPPORTED_RECOMMENDATION_ACTIONS = [ACTION_RECOMMENDATION_BOOKMARK,
                                    ACTION_RECOMMENDATION_REMOVE,
                                    ACTION_RECOMMENDATION_RECOMMEND,
                                    ACTION_RECOMMENDATION_NOT_RECOMMEND]

CATEGORY_MOVIE = "movies"
CATEGORY_RESTAURANT = "restaurants"
CATEGORY_VIDEO = "videos"

SUPPORTED_CATEGORIES = [CATEGORY_MOVIE, CATEGORY_RESTAURANT, CATEGORY_VIDEO]

USER_MOVIE_PROPERTY_TO_QUESTIONS_MAP = {
    "genre": "What genre movies do you prefer?",
    "type": "What types of movies do you prefer?"
}

USER_RESTAURANT_PROPERTY_TO_QUESTIONS_MAP = {
    "cuisine": "What cuisines you like?"
}

USER_VIDEO_PROPERTY_TO_QUESTIONS_MAP = {
    "" : ""
}

USER_MOVIE_PROPERTY_TO_ANSWER_MAP = {
    "genre_horror": "Horror",
    "genre_comedy": "Comedy",
    "genre_romantic": "Romcom",

    "type_bollywood": "Bollywood",
    "type_hollywood": "Hollywood",
    "type_foreign": "Foreign Languages"
}

USER_RESTAURANT_PROPERTY_TO_ANSWER_MAP = {
    "cuisine_italian" : "Italian",
    "cuisine_northindian": "North Indian",
    "cuisine_southindian": "South India",
    "cuisine_smallbites": "Small bites"
}

USER_VIDEO_PROPERTY_TO_ANSWER_MAP = {
    "" : ""
}

# Movie Genres
GENRE_HORROR = "horror"
GENRE_COMEDY = "comedy"
GENRE_CRIME = "crime"
GENRE_ROMANCE = "romance"
GENRE_MYSTERY = "mystery"
GENRE_THRILLER = "thriller"
GENRE_DRAMA = "drama"
GENRE_ACTION = "action"
GENRE_BIOGRAPHY = "biography"
GENRE_SCIFI = "sci-fi"
GENRE_HISTORY = "history",
GENRE_WAR = "war"

SUPPORTED_GENRES = [GENRE_ACTION, GENRE_BIOGRAPHY, GENRE_COMEDY, GENRE_CRIME, GENRE_DRAMA, GENRE_HISTORY,
                    GENRE_HORROR, GENRE_MYSTERY, GENRE_ROMANCE, GENRE_SCIFI, GENRE_THRILLER, GENRE_WAR]

GENRES_PROPERTY_TO_DISPLAY_MAP = {
    GENRE_HORROR: "Horror",
    GENRE_COMEDY: "Comedy",
    GENRE_CRIME: "Crime",
    GENRE_ROMANCE: "Romance",
    GENRE_MYSTERY: "Mystery",
    GENRE_THRILLER: "Thriller",
    GENRE_DRAMA: "Drama",
    GENRE_ACTION: "Action",
    GENRE_BIOGRAPHY: "Biography",
    GENRE_SCIFI: "Sci-fi",
    GENRE_HISTORY: "History",
    GENRE_WAR: "War"
}

# Movie Languages
LANGUAGE_ENGLISH = "english"
LANGUAGE_HINDI = "hindi"
LANGUAGE_OTHERS = "others"

SUPPORTED_LANGUAGES = [LANGUAGE_ENGLISH, LANGUAGE_HINDI]

LANGUAGES_PROPERTY_TO_DISPLAY_MAP = {
    LANGUAGE_ENGLISH: "English",
    LANGUAGE_HINDI: "Hindi",
    LANGUAGE_OTHERS: "Others"
}

# Movie years
YEAR_POST_2000 = "2000s"
YEAR_1980_TO_2000 = "1980-2000"
YEAR_PRE_1980s = "pre_1980s"

SUPPORTED_YEARS = [YEAR_POST_2000, YEAR_1980_TO_2000, YEAR_PRE_1980s]

YEARS_PROPERTY_TO_DISPLAY_MAP = {
    YEAR_POST_2000: "2000s till now",
    YEAR_1980_TO_2000: "1980 to 2000",
    YEAR_PRE_1980s: "Before 1980"
}

# On demand movie occasions
OCCASION_TYPE_BIRTHDAY = "birthday"
OCCASION_TYPE_DATE = "date"
OCCASION_TYPE_FAMILY_VIEWING = "family"
OCCASION_TYPE_FRIENDS_VIEWING = "friends"
OCCASION_TYPE_EVERYDAY = "everyday"

SUPPORTED_ON_DEMAND_MOVIE_OCCASIONS = [
    OCCASION_TYPE_BIRTHDAY,
    OCCASION_TYPE_DATE,
    OCCASION_TYPE_FAMILY_VIEWING,
    OCCASION_TYPE_FRIENDS_VIEWING,
    OCCASION_TYPE_EVERYDAY
]

# Recommendation frequency type
RECOMMENDATION_TYPE_DAILY = "daily"
RECOMMENDATION_TYPE_WEEKEND = "weekend"
SUPPORTED_RECOMMENDATION_FREQUENCIES = [RECOMMENDATION_TYPE_DAILY,
                                        RECOMMENDATION_TYPE_WEEKEND]


def get_questions_text(category, question_property):
    if category == CATEGORY_MOVIE:
        return USER_MOVIE_PROPERTY_TO_QUESTIONS_MAP.get(question_property)
    elif category == CATEGORY_RESTAURANT:
        return USER_RESTAURANT_PROPERTY_TO_QUESTIONS_MAP.get(question_property)
    elif category == CATEGORY_VIDEO:
        return USER_VIDEO_PROPERTY_TO_QUESTIONS_MAP.get(question_property)


def get_answer_text(category, question_property, answer_property):
    key = question_property + "_" + answer_property
    if category == CATEGORY_MOVIE:
        return USER_MOVIE_PROPERTY_TO_ANSWER_MAP.get(key)
    elif category == CATEGORY_RESTAURANT:
        return USER_RESTAURANT_PROPERTY_TO_ANSWER_MAP.get(key)
    elif category == CATEGORY_VIDEO:
        return USER_VIDEO_PROPERTY_TO_ANSWER_MAP.get(key)
