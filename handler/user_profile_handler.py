from handler.base_request_handler import BaseRequestHandler
from lib import http_response
from lib.user import UserClass
import json
import logging


class UserProfileHandler(BaseRequestHandler):

    def _get(self):
        ## Validate request
        userId = self.request.get('userId')

        user_profile = UserClass().get_user_profile_info(userId)

        http_response.set_200_response(self.response, user_profile)
        return

    def _put(self):
        ## Validate request

        userid = self.request.get('userId')
        action = self.request.get('action')

        if action == 'update_interest_categories':
            categories_string = self.request.get('categories')

            categories = categories_string.split(",")

            user_interests_categories = UserClass().update_user_interest_categories(userid=userid,
                                                                                    categories=categories)
            http_response.set_200_response(self.response, {'categories': user_interests_categories})
            return

        request_json = json.loads(self.request.body)

        first_name = request_json.get('first_name')
        last_name = request_json.get('last_name')
        email = request_json.get('email')
        phone_number = request_json.get('phone_number')

        UserClass().set_user_profile_data(userId=userid, first_name=first_name, last_name=last_name, email=email,
                                          phone_number=phone_number)

        http_response.set_200_response(self.response, {})
        return


class UserFirebaseTokenHandler(BaseRequestHandler):

    def _put(self):
        userid = self.request.get('userid')
        firebase_token = self.request.get('firebase_token')

        if userid:
            user = UserClass().get_user_by_id(userid)

        if not user:
            logging.error("User id: %s is invalid", userid)
            http_response.set_400_response(self.response, "User id: " + userid + " is invalid")
            return

        if firebase_token:
            user.firebase_token = firebase_token
            user.put()
            http_response.set_200_response(self.response, {"message": "Updated firebase token"})
            return

        http_response.set_400_response(self.response, "No firebase_token in request")

