
from clients.base_client import BaseClient
import json


class OMDBClient(BaseClient):

    API_KEY = "c0b0acb3"
    BASE_URL = "http://www.omdbapi.com/"

    def __init__(self):
        super(BaseClient, self).__init__()

    def get_movie_details(self, imdbId):
        self._url = self.BASE_URL + "?i=" + imdbId + "&apiKey=" + self.API_KEY
        self._headers = {"Accept": "application/json"}

        response = self.get()
        if response:
            response = json.loads(response)
            movie_details = self._parse_movie_details(response)
            return movie_details
        return None

    def _parse_movie_details(self, response):
        movie_details = {
            'name': response.get('Title'),
            'release_year': int(response.get('Year')),
            'runtime': response.get('Runtime'),
            'genres': [x.strip() for x in response.get('Genre').lower().split(',')],
            'director': [x.strip() for x in response.get('Director').split(',')],
            'writer': [x.strip() for x in response.get('Writer').split(',')],
            'actors': [x.strip() for x in response.get('Actors').split(',')],
            'plot': response.get('Plot'),
            'language': [x.strip() for x in response.get('Language').lower().split(',')],
            'rating': float(response.get('imdbRating')),
            'poster': response.get('Poster'),
            'type': response.get('Type')
        }
        return movie_details
