
from datastore import UserPersonalisations, User, PersonalisationQuestion
from lib import constants


class UserPersonalisationsClass(object):

    def get_personalisation_questions_and_responses(self, userid):
        user = User.get_by_id(int(userid))
        personalisation_json = {}
        if not user.user_selected_interests:
            return personalisation_json

        for category in user.user_selected_interests:
            personalisation_json[category] = self._get_category_personalisations(user, category)
        return personalisation_json

    def update_personalisation(self, userid, category, property, answers):
        user = User.get_by_id(int(userid))
        question = PersonalisationQuestion.query(PersonalisationQuestion.category == category,
                                                 PersonalisationQuestion.property == property).get()

        user_personalisation = UserPersonalisations.query(UserPersonalisations.question == question.key,
                                                          UserPersonalisations.user == user.key).get()

        if user_personalisation:
            user_personalisation.response_answers = answers
        else:
            user_personalisation = UserPersonalisations(category=category,
                                                        question=question.key,
                                                        user=user.key,
                                                        response_answers=answers)
        user_personalisation.put()

    def _get_category_personalisations(self, user, category):
        category_questions = PersonalisationQuestion.query(PersonalisationQuestion.category == category).fetch()
        questions_list = []

        for question in category_questions:
            personalisation = UserPersonalisations.query(UserPersonalisations.user == user.key,
                                                         UserPersonalisations.question == question.key).get()
            question_json = {
                'question_text': constants.get_questions_text(category, question.property),
                'property': question.property,
                'category': category,
                'id': question.key.id()
            }

            if personalisation:
                question_json['is_answered'] = True
                question_json['answer_options'] = []
                for answer_option in question.answer_options:
                    answer_option_json = {
                        'property': answer_option,
                        'answer_text': constants.get_answer_text(category, question.property, answer_option)
                    }
                    if answer_option in personalisation.response_answers:
                        answer_option_json['selected'] = True
                        question_json['answer_options'].append(answer_option_json)
                    else:
                        answer_option_json['selected'] = False
                        question_json['answer_options'].append(answer_option_json)
            else:
                question_json['is_answered'] = False
                question_json['answer_options'] = []
                for answer_option in question.answer_options:
                    answer_option_json = {
                        'selected': False,
                        'property': answer_option,
                        'answer_text': constants.get_answer_text(category, question.property, answer_option)
                    }
                    question_json['answer_options'].append(answer_option_json)

            questions_list.append(question_json)

            return questions_list
