
from clients.omdb_client import OMDBClient
from clients.just_watch_client import JustWatchClient
from datastore import Movie
from lib import constants
from google.cloud import ndb
import logging


class MovieClass(object):

    def add_movie(self, imdb_id, short_description=None, featured_in=None, available_at=None, curated_by=None,
                  trailer=None, google_score=None, reviews=None, tags=None):

        # Check movie exists
        movie = Movie.query(Movie.imdb_id == imdb_id).get()
        if not movie:
            movie = Movie(imdb_id=imdb_id)

        if short_description:
            movie.short_description = short_description
        if google_score:
            movie.google_score = google_score
        if trailer:
            movie.trailer = trailer
        if featured_in:
            if movie.featured_in is None:
                movie.featured_in = featured_in
            else:
                featured_in_json = movie.featured_in
                for key in featured_in.keys():
                    featured_in_json[key] = featured_in.get(key)
                movie.featured_in = featured_in_json
        if available_at:
            movie.available_at = available_at
            available_at_list = []
            for available_item in available_at.keys():
                available_at_list.append(available_item)
            movie.available_at_list = available_at_list

        if reviews:
            movie.reviews = reviews
        if tags:
            updated_tags = []
            if movie.tags:
                updated_tags.extend(movie.tags)
            updated_tags.extend(tags)
            movie.tags = updated_tags

        imdb_movie_details = self._get_movie_detils(imdb_id)

        if imdb_movie_details:
            movie.language = imdb_movie_details.get('language')
            movie.language_first = imdb_movie_details.get('language')[0]
            movie.genre = imdb_movie_details.get('genres')
            movie.imdb_rating = imdb_movie_details.get('rating')
            movie.director = imdb_movie_details.get('director')
            movie.writer = imdb_movie_details.get('writer')
            movie.actors = imdb_movie_details.get('actors')
            movie.type = imdb_movie_details.get('type')

            movie_release_year = imdb_movie_details.get('release_year')
            if movie_release_year:
                movie.year = imdb_movie_details.get('release_year')
                if movie_release_year < 1980:
                    movie.year_tags = [constants.YEAR_PRE_1980s]
                elif 1980 <= movie_release_year <= 2000:
                    movie.year_tags = [constants.YEAR_1980_TO_2000]
                elif movie_release_year > 2000:
                    movie.year_tags = [constants.YEAR_POST_2000]

            movie.poster = imdb_movie_details.get('poster')
            movie.plot = imdb_movie_details.get('plot')
            movie.name = imdb_movie_details.get('name')

            # Update streaming providers:
            movie_providers = JustWatchClient().get_available_providers_for_a_title_in_country(
                title=imdb_movie_details.get('name'),
                release_year=int(imdb_movie_details.get('release_year')),
                country_code="IN")
            movie.available_at = movie_providers
            movie.available_at_list = list(movie_providers.keys())

            movie.put()
        else:
            logging.error("Invalid imdbid %s for movie", imdb_id)
            raise ValueError("Invalid imdbid %s for movie", imdb_id)

    def get_movies(self, genres, years, languages, streaming_platforms):
        movie_query = Movie.query()

        if genres and isinstance(genres, list) and len(genres) > 0:
            movie_query = movie_query.filter(Movie.genre.IN(genres))
        if languages and isinstance(languages, list) and len(languages) > 0:
            movie_query = movie_query.filter(Movie.language_first.IN(languages))
        if years and isinstance(years, list) and len(years) > 0:
            if constants.YEAR_PRE_1980s in years and constants.YEAR_1980_TO_2000 in years and constants.YEAR_POST_2000 in years:
                pass
            elif constants.YEAR_PRE_1980s in years and constants.YEAR_1980_TO_2000 in years:
                movie_query = movie_query.filter(Movie.year < 2000)
            elif constants.YEAR_1980_TO_2000 in years and constants.YEAR_POST_2000 in years:
                movie_query = movie_query.filter(Movie.year >= 1980)
            elif constants.YEAR_PRE_1980s in years and constants.YEAR_POST_2000 in years:
                movie_query = movie_query.filter(ndb.OR(Movie.year < 1980, Movie.year >= 2000))
            elif constants.YEAR_PRE_1980s in years:
                movie_query = movie_query.filter(Movie.year < 1980)
            elif constants.YEAR_1980_TO_2000 in years:
                movie_query = movie_query.filter(Movie.year >= 1980, Movie.year < 2000)
            elif constants.YEAR_POST_2000 in years:
                movie_query = movie_query.filter(Movie.year >= 2000)
        if streaming_platforms and isinstance(streaming_platforms, list) and len(streaming_platforms) > 0:
            movie_query = movie_query.filter(Movie.available_at_list.IN(streaming_platforms))
        #movies = movie_query.order(Movie.year, -Movie.imdb_rating, -Movie.update_time).fetch(100)
        movies = movie_query.fetch(100)
        movies = sorted(movies, key=lambda movie: movie.google_score, reverse=True)
        return movies

    def get_movies_with_tags(self, genres, years, languages, streaming_platforms, tags):
        movie_query = Movie.query()
        if genres and isinstance(genres, list) and len(genres) > 0:
            movie_query = movie_query.filter(Movie.genre.IN(genres))
        if languages and isinstance(languages, list) and len(languages) > 0:
            movie_query = movie_query.filter(Movie.language_first.IN(languages))
        if years and isinstance(years, list) and len(years) > 0:
            movie_query = movie_query.filter(Movie.year_tags.IN(years))
        if streaming_platforms and isinstance(streaming_platforms, list) and len(streaming_platforms) > 0:
            movie_query = movie_query.filter(Movie.available_at_list.IN(streaming_platforms))
        if tags and isinstance(tags, list) and len(tags) > 0:
            movie_query = movie_query.filter(Movie.tags.IN(tags))
        movies = movie_query.fetch(100)
        movies = sorted(movies, key=lambda movie: movie.google_score, reverse=True)
        return movies

    def get_movies_with_tags_without_filters(self, tags):
        movie_query = Movie.query()
        if tags and isinstance(tags, list) and len(tags) > 0:
            movie_query = movie_query.filter(Movie.tags.IN(tags))
        movies = movie_query.fetch(100)
        movies = sorted(movies, key=lambda movie: movie.google_score, reverse=True)
        return movies

    def get_movies_without_filters(self):
        movie_query = Movie.query()
        movies = movie_query.fetch(100)
        movies = sorted(movies, key=lambda movie: movie.google_score, reverse=True)
        return movies

    def get_movie(self, movie_id):
        return Movie.get_by_id(movie_id)

    def _get_movie_detils(self, imdb_id):
        imdb_movie_details = OMDBClient().get_movie_details(imdbId=imdb_id)
        return imdb_movie_details
