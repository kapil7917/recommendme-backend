
from handler.base_request_handler import BaseRequestHandler
from lib import http_response
from lib.movie_profile import MovieProfileClass
from lib.user import UserClass
from lib.movie import MovieClass
from lib.user_movie_recommendation import UserMovieRecommendationClass
from lib.task_queue import TaskQueue
from lib.firebase_notification import send_fcm_message
import datetime

import logging
import json


def get_movie_notification_body(movies):
    notification_body = ""
    if movies and len(movies) > 0:
        main_reco = movies[0]
        notification_body += main_reco.name
        if main_reco.available_at_list and len(main_reco.available_at_list) > 0:
            # Add major providers name first
            if "Netflix" in main_reco.available_at_list:
                notification_body += " On Netflix"
            elif "Prime Video" in main_reco.available_at_list:
                notification_body += " On Amazon Prime Video"
            elif "Hotstar" in main_reco.available_at_list:
                notification_body += " On Hotstar"
            elif "Zee5" in main_reco.available_at_list:
                notification_body += " On Zee5"
            else:
                notification_body += " On " + main_reco.available_at_list[0]
        if len(movies) > 1:
            # Add text and others
            notification_body += " and others"
        return notification_body


def generate_movie_recommendation_for_user(user, movies_count=1):
    # Get movie profile
    movie_profile = MovieProfileClass().get_movie_profile(user=user.key)
    recommended_movies = []
    user_movie_reco_generated = False
    if movie_profile:
        genres = movie_profile.genres
        languages = movie_profile.languages
        years = movie_profile.years
        streaming_platforms = movie_profile.streaming_platforms
        send_surprise_reco = movie_profile.send_surprise_reco

        # Query movie table with filters
        movies = MovieClass().get_movies(genres=genres,
                                         languages=languages,
                                         years=years,
                                         streaming_platforms=streaming_platforms)

        for movie in movies:
            logging.debug("Checking movie %s if valid for user %s", movie.key, user.key)
            user_movie_recommendation = UserMovieRecommendationClass.get_user_movie_recommendation(
                user_key=user.key,
                movie_key=movie.key)
            if not user_movie_recommendation:
                if len(recommended_movies) < movies_count:
                    logging.debug("Adding movie recommendation %s for user %s", movie.key, user.key)
                    UserMovieRecommendationClass.add_user_movie_recommendation(user_key=user.key,
                                                                               movie_key=movie.key)
                    recommended_movies.append(movie)
                else:
                    break

        if len(recommended_movies) < movies_count and send_surprise_reco is True:
            logging.debug("Generating surprise movie recommendation for user %s", user.key)
            # Query movie table without filters
            movies = MovieClass().get_movies_without_filters()
            for movie in movies:
                logging.debug("Checking movie %s if valid for user %s", movie.key, user.key)
                user_movie_recommendation = UserMovieRecommendationClass.get_user_movie_recommendation(
                    user_key=user.key,
                    movie_key=movie.key)
                if not user_movie_recommendation:
                    if len(recommended_movies) < movies_count:
                        logging.debug("Adding movie recommendation %s for user %s", movie.key, user.key)
                        UserMovieRecommendationClass.add_user_movie_recommendation(user_key=user.key,
                                                                                   movie_key=movie.key)
                        recommended_movies.append(movie)
                    else:
                        break

        if len(recommended_movies) > 0:
            # Send push notification that recommendations are generated for user
            send_fcm_message(user.firebase_token,
                             title="We Recommend",
                             body=get_movie_notification_body(recommended_movies),
                             image_url=recommended_movies[0].poster)
        else:
            logging.warning("No movie recommendation generated for user %s", user.key)


class GenerateInitialMovieRecommendationsHandler(BaseRequestHandler):

    def _post(self):
        request_json = json.loads(self.request.body)
        userid = request_json.get("userid")
        user = UserClass().get_user_by_id(userid=userid)
        if not UserMovieRecommendationClass.get_movie_recommendations_by_user_key(user_key=user.key):
            logging.debug("Generating movie recommendation for user: %s", user.key)
            generate_movie_recommendation_for_user(user=user, movies_count=3)
        http_response.set_200_response(self.response, {"message": "added recommendations for user"})


class GenerateMovieRecommendationsHandler(BaseRequestHandler):

    def _post(self):
        # Get users
        users, cursor = UserClass.get_users()
        for user in users:
            logging.debug("Generating movie recommendation for user: %s", user.key)
            last_movie_recommendation = UserMovieRecommendationClass.get_last_recommendation_for_user(user.key)
            if last_movie_recommendation and last_movie_recommendation.create_time + datetime.timedelta(hours=22) < datetime.datetime.utcnow():
                generate_movie_recommendation_for_user(user=user)
            else:
                logging.debug("Recommendations already generated for user %s in last 24 hours", user.key)
        http_response.set_200_response(self.response, {"message": "added recommendations for user"})


class GenerateMovieRecommendationsCronHandler(BaseRequestHandler):

    def _get(self):
        logging.info("Started cron job to generate movie reco")
        task_queue = TaskQueue(queue="test-queue")
        task_queue.add_task(payload=json.dumps({}),
                            task_url="/generate/user/movie/recommendations")

        http_response.set_200_response(self.response, "Started cron to generate movie recommendations")
