import urllib3


class BaseClient(object):

    def __init__(self):
        self._url = None
        self._headers = None

    def get(self):
        http = urllib3.PoolManager(
            cert_reqs='CERT_REQUIRED',
            ca_certs='/etc/ssl/certs/ca-certificates.crt')
        http_response = http.request(method='GET', url=self._url, headers=self._headers)
        response_data = http_response.data
        return response_data
