
from handler.base_request_handler import BaseRequestHandler
from handler.base_request_handler import is_valid_request
from lib import http_response
from lib.user import UserClass

import json


class FeedbackHandler(BaseRequestHandler):

    def _post(self):
        userid = self.request.get('userid')
        access_token = self.request.get('access_token')

        request_json = json.loads(self.request.body)
        feedback_text = request_json.get('feedback_text')

        # Validate user and authorize
        if not is_valid_request(userid, access_token):
            http_response.set_410_response(self.response, "Access token/user id is invalid")
            return

        UserClass().add_user_feedback(userid=userid, feedback_text=feedback_text)

        http_response.set_200_response(self.response, {"message": "Added feedback successfully"})
