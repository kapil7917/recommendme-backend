
from clients.base_client import BaseClient
import logging
import json


class FacebookClient(BaseClient):

    GRAPH_REQUEST_BASE_URL = "https://graph.facebook.com/v4.0"
    USER_REQUEST_API_PATH = "/me"

    def __init__(self):
        super(BaseClient, self).__init__()

    def is_access_token_valid(self, access_token, facebook_userid):
        self._url = self.GRAPH_REQUEST_BASE_URL + self.USER_REQUEST_API_PATH + "?access_token=" + access_token
        self._headers = { "Accept": "application/json"}
        response = self.get()
        if response:
            response = json.loads(response)
            if response.get('id') and response.get('id') == facebook_userid:
                return True
        return False

    def get_user_friends_details(self, access_token):
        self._url = self.GRAPH_REQUEST_BASE_URL + self.USER_REQUEST_API_PATH + "?access_token=" + access_token + "&fields=friends"
        self._headers = { "Accept": "application/json"}
        response = self.get()
        if response:
            response = json.loads(response)
            friends_details = self._parse_friends_details(response)
            return friends_details
        return None

    def _parse_friends_details(self, response):
        friends_ids_list = []
        if response.get('friends'):
            friends_data = response.get('friends').get('data')
            if friends_data and len(friends_data) > 0:
                for friend in friends_data:
                    friends_ids_list.append(friend.get('id'))
            else:
                logging.debug("Received empty friends info %s", friends_data)
        return {'ids': friends_ids_list}
