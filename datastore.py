from google.cloud import ndb


class User(ndb.Expando):
    first_name = ndb.StringProperty()
    last_name = ndb.StringProperty()
    gender = ndb.StringProperty()
    phone_number = ndb.StringProperty()
    email = ndb.StringProperty(required=True)
    country = ndb.StringProperty()
    city = ndb.StringProperty()
    user_type = ndb.StringProperty()
    facebook_userid = ndb.StringProperty()
    firebase_token = ndb.StringProperty()
    fb_access_token = ndb.StringProperty(default=None)
    ## interests per category in JSON - can be changed as per needs
    user_selected_interests = ndb.JsonProperty()
    facebook_interests = ndb.StringProperty(repeated=True)
    create_time = ndb.DateTimeProperty(auto_now_add=True)
    update_time = ndb.DateTimeProperty(auto_now=True)


class Recommendation(ndb.Expando):
    category = ndb.StringProperty()
    title = ndb.StringProperty(required=True)
    short_description = ndb.StringProperty(required=True)
    description = ndb.StringProperty()
    city = ndb.StringProperty()
    tags = ndb.StringProperty(repeated=True)
    details = ndb.JsonProperty(default={})
    created_by = ndb.KeyProperty(User)
    create_time = ndb.DateTimeProperty(auto_now_add=True)
    update_time = ndb.DateTimeProperty(auto_now=True)


class UserRecommendations(ndb.Expando):
    user = ndb.KeyProperty(User, required=True)
    recommendations = ndb.StringProperty(repeated=True)
    create_time = ndb.DateTimeProperty(auto_now=True)


class Device(ndb.Expando):
    user = ndb.KeyProperty(User, required=True)
    device_type = ndb.StringProperty()
    create_time = ndb.DateTimeProperty(auto_now_add=True)
    update_time = ndb.DateTimeProperty(auto_now=True)


class PersonalisationQuestion(ndb.Expando):
    answer_options = ndb.StringProperty(repeated=True)
    category = ndb.StringProperty(required=True)
    property = ndb.StringProperty(required=True)
    type = ndb.StringProperty() # Single option or multiple options


class UserPersonalisations(ndb.Expando):
    user = ndb.KeyProperty(User, required=True)
    question = ndb.KeyProperty(PersonalisationQuestion, required=True)
    response_answers = ndb.StringProperty(repeated=True)
    category = ndb.StringProperty(required=True)
    create_time = ndb.DateTimeProperty(auto_now_add=True)
    update_time = ndb.DateTimeProperty(auto_now=True)


class MovieProfile(ndb.Expando):
    user = ndb.KeyProperty(User, required=True)
    genres = ndb.StringProperty(repeated=True)
    languages = ndb.StringProperty(repeated=True)
    years = ndb.StringProperty(repeated=True)
    streaming_platforms = ndb.StringProperty(repeated=True)
    send_surprise_reco = ndb.BooleanProperty(default=True)
    interest_rating = ndb.IntegerProperty(default=0)


class Movie(ndb.Expando):
    name = ndb.StringProperty()
    short_description = ndb.StringProperty()
    plot = ndb.StringProperty()
    genre = ndb.StringProperty(repeated=True)
    year = ndb.IntegerProperty()
    language = ndb.StringProperty(repeated=True)
    language_first = ndb.StringProperty()
    featured_in = ndb.JsonProperty(default={})
    available_at = ndb.JsonProperty(default={})
    available_at_list = ndb.StringProperty(repeated=True)
    justwatch_available_at = ndb.JsonProperty(default={})
    justwatch_available_at_list = ndb.StringProperty(repeated=True)
    reviews = ndb.StringProperty(repeated=True)
    imdb_id = ndb.StringProperty()
    imdb_rating = ndb.FloatProperty(default=0.0)
    trailer = ndb.StringProperty()
    poster = ndb.StringProperty()
    type = ndb.StringProperty()
    google_score = ndb.IntegerProperty(default=0)
    director = ndb.StringProperty(repeated=True)
    writer = ndb.StringProperty(repeated=True)
    actors = ndb.StringProperty(repeated=True)
    tags = ndb.StringProperty(repeated=True)
    year_tags = ndb.StringProperty(repeated=True)
    curated_by = ndb.StringProperty()
    create_time = ndb.DateTimeProperty(auto_now_add=True)
    update_time = ndb.DateTimeProperty(auto_now=True)


class JustWatchMovie(ndb.Expando):
    imdb_id = ndb.StringProperty()
    name = ndb.StringProperty()
    year = ndb.IntegerProperty()
    available_at = ndb.JsonProperty(default={})
    available_at_list = ndb.StringProperty(repeated=True)


class UserMovieRecommendation(ndb.Expando):
    user = ndb.KeyProperty(User, required=True)
    movie = ndb.KeyProperty(Movie, required=True)
    is_bookmarked = ndb.BooleanProperty(default=False)
    is_recommended = ndb.BooleanProperty(default=False)
    is_not_recommended = ndb.BooleanProperty(default=False)
    create_time = ndb.DateTimeProperty(auto_now_add=True)
    update_time = ndb.DateTimeProperty(auto_now=True)


class RecommendationActionEvents(ndb.Expando):
    recommendation = ndb.KeyProperty(UserMovieRecommendation, required=True)
    user = ndb.KeyProperty(User, required=True)
    facebook_userid = ndb.StringProperty()
    action = ndb.StringProperty()
    create_time = ndb.DateTimeProperty(auto_now_add=True)
    update_time = ndb.DateTimeProperty(auto_now=True)


class UserOnDemandMovieRequests(ndb.Expando):
    user = ndb.KeyProperty(User, required=True)
    event_type = ndb.StringProperty()
    status = ndb.StringProperty()
    create_time = ndb.DateTimeProperty(auto_now_add=True)
    update_time = ndb.DateTimeProperty(auto_now=True)


class UserFeedback(ndb.Expando):
    user = ndb.KeyProperty(User, required=True)
    feedback_text = ndb.TextProperty()
