from handler.base_request_handler import BaseRequestHandler
from lib import http_response
from lib.user_personalisations import UserPersonalisationsClass
import json


class UserPersonalisationHandler(BaseRequestHandler):

    def _get(self):
        ## Validate request
        userid = self.request.get('userid')
        personalisation_questions = UserPersonalisationsClass().get_personalisation_questions_and_responses(userid)

        http_response.set_200_response(self.response, personalisation_questions)
        return

    def _put(self):
        ## Validate request

        userid = self.request.get('userid')

        request_json = json.loads(self.request.body)

        question_property = request_json.get('question_property')
        response_answers = request_json.get('answers')
        category = request_json.get('category')

        UserPersonalisationsClass().update_personalisation(userid=userid,
                                                           property=question_property,
                                                           answers=response_answers,
                                                           category=category)
        http_response.set_200_response(self.response, {"message": "Updated user question response"})
        return
