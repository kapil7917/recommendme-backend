#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

from flask import Flask, jsonify
import webapp2
from handler import recommendations_history_handler
from handler import user_profile_handler, user_login_handler, user_logout_handler, recommendations_handler, \
    user_personalisation_handler, feedback_handler, warmup_handler
from handler.movies import movie_profile_handler, movie_handler
from handler.crons import generate_movie_recos_cron, update_movie_providers
from handler.tasks import on_demand_recommendations_task_handler

# app = Flask(__name__)
#
#
# @app.route('/')
# def root():
#     return jsonify({})
#
#
# @app.route('/user/login', methods=['POST'])
# def login():
#     return jsonify({})


class MainHandler(webapp2.RequestHandler):
    def get(self):
        self.response.write('Hello world!')


app = webapp2.WSGIApplication([
    ('/', MainHandler),
    ('/user/logout', user_logout_handler.UserLogoutHandler),
    ('/user/login', user_login_handler.UserLoginHandler),
    ('/user/profile', user_profile_handler.UserProfileHandler),
    ('/user/firebase_token', user_profile_handler.UserFirebaseTokenHandler),
    ('/user/recommendations/history', recommendations_history_handler.RecommendationsHistoryHandler),
    ('/user/recommendations', recommendations_handler.UserRecommendationsHandler),
    ('/user/ondemand/recommendations', recommendations_handler.OnDemandRecommendationsHandler),
    ('/recommendation', recommendations_handler.RecommendationHandler),
    ('/user/recommendation', recommendations_handler.RecommendationActionHandler),
    ('/user/personalisation', user_personalisation_handler.UserPersonalisationHandler),
    ('/user/movie/profile', movie_profile_handler.MovieProfileHandler),
    ('/generate/user/movie/recommendations', generate_movie_recos_cron.GenerateMovieRecommendationsHandler),
    ('/generate/user/initial/movie/recommendations', generate_movie_recos_cron.GenerateInitialMovieRecommendationsHandler),
    ('/generate/user/movie/on_demand/recommendations', on_demand_recommendations_task_handler.OnDemandRecommendationsTaskHandler),
    ('/movie', movie_handler.MovieHandler),
    ('/feedback', feedback_handler.FeedbackHandler),
    ('/cron/generate_movie_recos', generate_movie_recos_cron.GenerateMovieRecommendationsCronHandler),
    ('/cron/update_movie_providers', update_movie_providers.UpdateMovieProvidersCronHandler),
    ('/update/movie/providers', update_movie_providers.UpdateMovieProvidersHandler),
    ('/movie/providers', movie_handler.MovieProviderHandler),
    ('/_ah/warmup', warmup_handler.WarmupHandler)
], debug=True)
