import json

from handler.base_request_handler import BaseRequestHandler

from lib import http_response
from lib.recommendation import RecommendationClass
from lib.movie import MovieClass
from clients.just_watch_client import JustWatchClient
from datastore import Movie


class MovieHandler(BaseRequestHandler):

    def _post(self):
        request_json = json.loads(self.request.body)

        short_description = request_json.get('short_description')
        imdb_id = request_json.get('imdb_id')
        featured_list = request_json.get('featured_list')
        available_at = request_json.get('available_at_list')
        trailer = request_json.get('trailer_link')
        google_score = request_json.get('google_score')
        reviews = request_json.get('reviews')
        tags = request_json.get('tags')

        try:
            if not imdb_id:
                raise ValueError("IMDB id not present")

            MovieClass().add_movie(imdb_id=imdb_id,
                                   short_description=short_description,
                                   featured_in=featured_list,
                                   available_at=available_at,
                                   trailer=trailer,
                                   google_score=google_score,
                                   reviews=reviews,
                                   tags=tags)
        except ValueError as e:
            http_response.set_400_response(self.response, e)

        http_response.set_200_response(self.response, {'recommendation_added': True})
        return


class MovieProviderHandler(BaseRequestHandler):

    def _get(self):
        movie_title = self.request.get('movie_title')
        year = self.request.get('release_year')

        http_response.set_200_response_2(self.response, JustWatchClient().get_available_providers_for_a_title_in_country(title=movie_title,
                                                                                                                         release_year=int(year),
                                                                                                                         country_code="IN"))
        return
