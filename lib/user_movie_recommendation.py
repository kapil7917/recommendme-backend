
from datastore import UserMovieRecommendation
from clients.facebook_client import FacebookClient
from clients.omdb_client import OMDBClient
from lib import constants

import logging


class UserMovieRecommendationClass(object):

    CATEGORY_MOVIE = "movie"

    def get_by_id(self, recommendationid):
        return UserMovieRecommendation.get_by_id(recommendationid)

    @staticmethod
    def get_user_movie_recommendation(user_key, movie_key):
        return UserMovieRecommendation.query(UserMovieRecommendation.user == user_key,
                                             UserMovieRecommendation.movie == movie_key).get()

    @staticmethod
    def get_last_recommendation_for_user(user_key):
        return UserMovieRecommendation.query(UserMovieRecommendation.user == user_key).order(-UserMovieRecommendation.create_time).get()

    @staticmethod
    def get_movie_recommendations_by_user_key(user_key):
        return UserMovieRecommendation.query(UserMovieRecommendation.user == user_key).get()

    def get_user_movie_recommendations(self, user_key, is_bookmarked=False, is_recommended=False, is_not_recommended=False, cursor=None):
        if cursor and isinstance(cursor, str) and cursor != '' and cursor.isdigit():
            cursor = int(cursor)
        else:
            cursor = 0

        limit = 10
        recommendations = UserMovieRecommendation.query(UserMovieRecommendation.user == user_key,
                                                        UserMovieRecommendation.is_bookmarked == is_bookmarked,
                                                        UserMovieRecommendation.is_recommended == is_recommended,
                                                        UserMovieRecommendation.is_not_recommended == is_not_recommended)\
             .order(-UserMovieRecommendation.create_time).fetch(limit=limit, offset=cursor)

        if recommendations and len(recommendations) == limit:
            next_curs = str(cursor+limit)
            more = True
        else:
            next_curs = None
            more = False

        return recommendations, next_curs, more

    @staticmethod
    def get_movie_recommendations(movie_key):
        return UserMovieRecommendation.query(UserMovieRecommendation.movie == movie_key,
                                             UserMovieRecommendation.is_recommended == True).fetch()

    @staticmethod
    def add_user_movie_recommendation(user_key, movie_key):
        user_movie_recommendation = UserMovieRecommendation()
        user_movie_recommendation.movie = movie_key
        user_movie_recommendation.user = user_key
        user_movie_recommendation.put()

    def generate_user_movie_recommendation(self, recommendation):
        movie_key = recommendation.movie
        movie = movie_key.get()
        if movie:
            movie_recommendation = self._add_recommendation_base_info(recommendation=recommendation,
                                                                      title=movie.name,
                                                                      short_description=movie.short_description)

            movie_recommendation['primary_image'] = movie.poster
            movie_recommendation['plot'] = movie.plot

            genres = []
            for genre in movie.genre:
                if genre.strip() in constants.GENRES_PROPERTY_TO_DISPLAY_MAP.keys():
                    genres.append(constants.GENRES_PROPERTY_TO_DISPLAY_MAP.get(genre.strip()))

            movie_recommendation['genres'] = genres
            movie_recommendation['rating'] = str(movie.imdb_rating)
            movie_recommendation['release_year'] = str(movie.year)
            movie_recommendation['imdb_id'] = movie.imdb_id
            movie_recommendation['language'] = movie.language[0]
            movie_recommendation['director'] = movie.director
            movie_recommendation['writer'] = movie.writer
            movie_recommendation['actors'] = movie.actors
            movie_recommendation['type'] = movie.type
            movie_recommendation['available_at'] = movie.available_at
            movie_recommendation['featured_in'] = movie.featured_in
            movie_recommendation['trailer'] = movie.trailer

            return movie_recommendation
        else:
            logging.warning("Recommendation details not available for movie %s", recommendation.key)
            return None

    def _add_recommendation_base_info(self, recommendation, title=None, short_description=None):
        if not short_description:
            # Passing empty for now if none
            short_description = ""
        return {
                'id': recommendation.key.id(),
                'category': self.CATEGORY_MOVIE,
                'title': title,
                'short_description': short_description,
                'is_bookmarked': recommendation.is_bookmarked,
                'is_recommendation_provided': recommendation.is_recommended or recommendation.is_not_recommended
        }
