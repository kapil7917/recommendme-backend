import datetime
import json

from google.protobuf import timestamp_pb2
from google.cloud import tasks_v2


class TaskQueue(object):
    def __init__(self, queue, location="europe-west1", project="recommendme-dev-243115"):
        self._queue = queue
        self._project = project
        self._location = location
        self._task_client = tasks_v2.CloudTasksClient()

    def get_queue_path(self):
        return self._task_client.queue_path(self._project, self._location, self._queue)

    def add_task(self, payload, task_url, scheduled_time=None, time_delta_seconds=0):
        assert isinstance(payload, str)
        if payload is None:
            payload = json.dumps({})

        headers = {

        }
        task = {
            "app_engine_http_request": {
                "http_method": "POST",
                "relative_uri": task_url,
                "body": payload.encode(),
                "headers": headers
            }
        }

        task_scheduled_time = scheduled_time
        if scheduled_time is None:
            task_scheduled_time = datetime.datetime.utcnow() + datetime.timedelta(seconds=time_delta_seconds)
        timestamp = timestamp_pb2.Timestamp()
        timestamp.FromDatetime(task_scheduled_time)
        response = self._task_client.create_task(self.get_queue_path(), task)
        return response
