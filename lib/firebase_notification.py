import logging
import firebase_admin
import os
import traceback
from firebase_admin import messaging
from firebase_admin import credentials

cred = credentials.Certificate(os.environ.get('SERVICE_ACCOUNT_FILE'))
firebase_admin.initialize_app(cred)

PROJECT_ID = os.environ.get('PROJECT_ID')
BASE_URL = 'https://fcm.googleapis.com'
FCM_ENDPOINT = 'v1/projects/' + PROJECT_ID + '/messages:send'
FCM_URL = BASE_URL + '/' + FCM_ENDPOINT
SCOPES = ['https://www.googleapis.com/auth/firebase.messaging']


def send_fcm_message(firebase_token, title, body, image_url=None):
    try:
        message = messaging.Message(
            notification=messaging.Notification(
                title=title,
                body=body,
                image=image_url
            ),
            token=firebase_token,
        )
        response = messaging.send(message)
        logging.debug("Successfully sent message: %s", response)
    except Exception:
        logging.error("Failed to send message for token %s", firebase_token)
        traceback.print_stack()
