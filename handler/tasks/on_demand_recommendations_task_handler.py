
from handler.base_request_handler import BaseRequestHandler
from lib.movie import MovieClass
from lib.movie_profile import MovieProfileClass
from lib.user import UserClass
from lib.user_movie_recommendation import UserMovieRecommendationClass
from lib.firebase_notification import send_fcm_message
from datastore import UserOnDemandMovieRequests
import json


class OnDemandRecommendationsTaskHandler(BaseRequestHandler):

    MAX_RECOMMENDATIONS_COUNT = 3

    def _post(self):
        request_json = json.loads(self.request.body)
        userid = request_json.get('userid')
        event_type = request_json.get('event_type')

        user = UserClass().get_user_by_id(userid=userid)

        # Update requests table
        user_on_demand_movie_requests = UserOnDemandMovieRequests(user=user.key,
                                                                  event_type=event_type,
                                                                  status="pending")
        user_on_demand_movie_requests.put()

        # User's movie profile:
        movie_profile = MovieProfileClass().get_movie_profile(user=user.key)

        movies = MovieClass().get_movies_with_tags(genres=movie_profile.genres,
                                                   years=movie_profile.years,
                                                   streaming_platforms=movie_profile.streaming_platforms,
                                                   languages=movie_profile.languages,
                                                   tags=[event_type])

        recommendations_generated = False
        movie_recommendations = []

        for movie in movies:
            user_movie_recommendation = UserMovieRecommendationClass().get_user_movie_recommendation(user_key=user.key,
                                                                                                     movie_key=movie.key)
            if not user_movie_recommendation:
                movie_recommendations.append(movie)
                if len(movie_recommendations) == self.MAX_RECOMMENDATIONS_COUNT:
                    recommendations_generated = True
                    break

        if recommendations_generated is False:
            movies = MovieClass().get_movies_with_tags_without_filters(tags=[event_type])
            for movie in movies:
                user_movie_recommendation = UserMovieRecommendationClass().get_user_movie_recommendation(
                    user_key=user.key,
                    movie_key=movie.key)
                if not user_movie_recommendation:
                    movie_recommendations.append(movie)
                    if len(movie_recommendations) == self.MAX_RECOMMENDATIONS_COUNT:
                        recommendations_generated = True
                        break
                        
        if len(movie_recommendations) > 0:
            # Add movie to user movie recommendations
            for movie in movie_recommendations:
                UserMovieRecommendationClass().add_user_movie_recommendation(user_key=user.key,
                                                                             movie_key=movie.key)
            # Update on-demand request status
            user_on_demand_movie_requests.status = "complete"
            user_on_demand_movie_requests.put()

            # Send push notification that recommendations are generated for user
            send_fcm_message(user.firebase_token, title="Movie Recommendations",
                             body="Your personalized movie recommendations are ready")

        else:
            # Send notification on couldn't generate
            send_fcm_message(user.firebase_token, title="Movie Recommendations",
                             body="Sorry! We couldn't find any movie recommendations for you. We are working hard on adding more movies.")
