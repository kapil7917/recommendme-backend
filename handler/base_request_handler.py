from google.cloud import ndb
from lib.user import UserClass
import webapp2

client = ndb.Client()


def is_valid_request(userid, access_token):
    user = UserClass().get_user_by_id(userid=userid)
    if user.fb_access_token and user.fb_access_token == access_token:
        return True
    return False


class BaseRequestHandler(webapp2.RequestHandler):

    def __init__(self, request, response):
        super(BaseRequestHandler, self).__init__(request, response)
        self._http_type = None

    def initialize(self, request, response):
        return super(BaseRequestHandler, self).initialize(request, response)

    def _get(self):
        pass

    def _post(self):
        pass

    def _put(self):
        pass

    def setup_context(self):
        with client.context():
            if self._http_type == "GET":
                return self._get()
            elif self._http_type == "POST":
                return self._post()
            elif self._http_type == "PUT":
                return self._put()

    def get(self):
        self._http_type = "GET"
        return self.setup_context()

    def post(self):
        self._http_type = "POST"
        return self.setup_context()

    def put(self):
        self._http_type = "PUT"
        return self.setup_context()
