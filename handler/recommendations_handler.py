import json

from handler.base_request_handler import BaseRequestHandler
from handler.base_request_handler import is_valid_request

from lib import http_response
from lib.recommendation import RecommendationClass
from lib.recommendation_action_events import RecommendationActionEventsClass
from lib.user import UserClass
from lib import constants
from lib.task_queue import TaskQueue
from datastore import UserMovieRecommendation
from lib.firebase_notification import send_fcm_message

import logging


class RecommendationHandler(BaseRequestHandler):

    def _post(self):

        userid = self.request.get('userid')
        # creator = User.get_by_id(userid)

        # ZomatoClient().get_restaurant_images()
        # return

        request_json = json.loads(self.request.body)

        category = request_json.get('category')

        if category == "Youtube Video":
            title = request_json.get('title')
            description = request_json.get('description')
            tags = request_json.get('tags')
            youtube_video_link = request_json.get('youtube_video_link')
            # RecommendationClass().add_youtube_recommendation(title=title, description=description, tags=tags,
            #                                                youtube_link=youtube_video_link, created_by=creator)
        elif category == "restaurant":
            title = request_json.get('title')
            short_description = request_json.get('short_description')
            restaurant_id = request_json.get('restaurant_id')
            RecommendationClass(user=None, creator=None).add_restaurant_recommendation(title=title,
                                                                                       short_description=short_description,
                                                                                       restaurant_id=restaurant_id)
        elif category == "movie":
            title = request_json.get('title')
            short_description = request_json.get('short_description')
            movie_id = request_json.get('movie_id')
            RecommendationClass(user=None, creator=None).add_movie_recommendation(title=title,
                                                                                  short_description=short_description,
                                                                                  movie_id=movie_id)
        else:
            pass

        http_response.set_200_response(self.response, {'recommendation_added': True})
        return


class OnDemandRecommendationsHandler(BaseRequestHandler):

    def _post(self):
        userid = self.request.get('userid')
        access_token = self.request.get('access_token')
        event_type = self.request.get('event_type')

        if not event_type or event_type not in constants.SUPPORTED_ON_DEMAND_MOVIE_OCCASIONS:
            http_response.set_400_response(self.response, "Event Type: " + event_type + " is invalid")
            return

        # Validate user and authorize
        if not is_valid_request(userid, access_token):
            http_response.set_410_response(self.response, "Access token/user id is invalid")
            return

        # Generate movies with taking user preferences and tags in consideration
        task_queue = TaskQueue(queue="test-queue")
        task_queue.add_task(payload=json.dumps({"userid": userid,
                                                "event_type": event_type}),
                            task_url="/generate/user/movie/on_demand/recommendations")

        http_response.set_200_response(self.response, {"message": "On demand movie recommendations request is being processed."})
        return


class UserRecommendationsHandler(BaseRequestHandler):

    def _get(self):
        # Request
        userid = self.request.get('userid')
        filter = self.request.get('filter')
        access_token = self.request.get('access_token')
        cursor = self.request.get('cursor')
        is_bookmarked = False

        # Validate user and authorize
        if not is_valid_request(userid, access_token):
            http_response.set_410_response(self.response, "Access token/user id is invalid")
            return

        user = None
        if userid:
            user = UserClass().get_user_by_id(userid)

        if not user:
            logging.error("User id: %s is invalid", userid)
            http_response.set_400_response(self.response, "User id: " + userid + " is invalid")
            return

        if filter == "bookmarks":
            is_bookmarked = True
            
        # Recommendation generator logic
        recommendations, next_curs, more = RecommendationClass(user=None, creator=None).get_recommendations_for_user(user=user,
                                                                                                    is_bookmarked=is_bookmarked,
                                                                                                    access_token=access_token,
                                                                                                    cursor=cursor)
        http_response.set_200_response(self.response, {"recommendations": recommendations,
                                                       "next_curs": next_curs,
                                                       "more": more})
        return


class RecommendationActionHandler(BaseRequestHandler):

    def _post(self):
        userid = self.request.get('userid')
        action = self.request.get('action')
        access_token = self.request.get('access_token')
        recommendationid = self.request.get('recommendationid')
        recommendation_type = self.request.get('recommendation_type')

        # Validate user and authorize
        if not is_valid_request(userid, access_token):
            http_response.set_410_response(self.response, "Access token/user id is invalid")
            return

        user = UserClass().get_user_by_id(userid)
        if not user:
            http_response.set_400_response(self.response, "User id: " + userid + " is invalid")
            return

        # Validate recommendation
        recommendation = RecommendationClass().get_recommendation(recommendation_id=int(recommendationid),
                                                                  recommendation_type=recommendation_type)
        if not recommendation:
            http_response.set_400_response(self.response, "Recommendation id: " + recommendationid + " is invalid")
            return

        logging.debug("Action event: %s from user %s for recommendation %s with type %s", action, userid, recommendationid, recommendation_type)

        if action in constants.SUPPORTED_RECOMMENDATION_ACTIONS:
            RecommendationActionEventsClass().create_recommendation_action_event(recommendation=recommendation,
                                                                                 user=user,
                                                                                 action=action)

            if action == constants.ACTION_RECOMMENDATION_REMOVE:
                recommendation.is_bookmarked = False
            elif action == constants.ACTION_RECOMMENDATION_BOOKMARK:
                recommendation.is_bookmarked = True
            elif action == constants.ACTION_RECOMMENDATION_RECOMMEND:
                recommendation.is_recommended = True
            elif action == constants.ACTION_RECOMMENDATION_NOT_RECOMMEND:
                recommendation.is_not_recommended = True

            recommendation.put()
        else:
            http_response.set_400_response("Invalid action param: " + action)
            return

        http_response.set_200_response(self.response, {"message": "Successfully updated"})
