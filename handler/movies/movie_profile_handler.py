
from handler.base_request_handler import BaseRequestHandler
from handler.base_request_handler import is_valid_request
from lib.movie_profile import MovieProfileClass
from lib.user import UserClass
from lib import http_response
from lib.task_queue import TaskQueue

import logging
import json


class MovieProfileHandler(BaseRequestHandler):

    def _get(self):
        userid = self.request.get('userid')
        access_token = self.request.get('access_token')

        # Validate user and authorize
        if not is_valid_request(userid, access_token):
            http_response.set_410_response(self.response, "Access token/user id is invalid")
            return

        user = UserClass().get_user_by_id(userid=userid)
        if not user:
            http_response.set_400_response(self.response, error_message="incorrect userid")
            return

        movie_profile_json = MovieProfileClass.get_movie_profile_json(user.key)

        http_response.set_200_response(self.response, json_data=movie_profile_json)

    def _post(self):

        # later update put/post where required
        userid = self.request.get('userid')
        access_token = self.request.get('access_token')

        # Validate user and authorize
        if not is_valid_request(userid, access_token):
            http_response.set_410_response(self.response, "Access token/user id is invalid")
            return

        request_json = json.loads(self.request.body)
        movie_interest_rating = request_json.get('movie_interest_rating')
        genres = request_json.get('genres')
        languages = request_json.get('languages')
        years = request_json.get('years')
        streaming_platforms = request_json.get('streaming_platforms')
        send_surprise_reco = request_json.get('send_surprise_reco')


        # Validate request
        user = UserClass().get_user_by_id(userid=userid)
        if not user:
            http_response.set_400_response(self.response, error_message="incorrect userid")
            return

        MovieProfileClass.update_movie_profile(user=user.key,
                                               genres=genres,
                                               languages=languages,
                                               years=years,
                                               interest_rating=movie_interest_rating,
                                               streaming_platforms=streaming_platforms,
                                               send_surprise_reco=send_surprise_reco)

        logging.info("Started cron job to generate movie reco")
        task_queue = TaskQueue(queue="test-queue")
        task_queue.add_task(payload=json.dumps({"userid": userid}),
                            task_url="/generate/user/initial/movie/recommendations")

        http_response.set_200_response(self.response, {"message": "Updated movie profile successfully"})
