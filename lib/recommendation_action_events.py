from datastore import RecommendationActionEvents


class RecommendationActionEventsClass(object):

    def create_recommendation_action_event(self, recommendation, user, action):
        recommendation_history = RecommendationActionEvents(
            action=action,
            recommendation=recommendation.key,
            user=user.key,
            facebook_userid=user.facebook_userid,
        )
        recommendation_history.put()

    def get_action_events_for_recommendation(self, recommendation_key, action):
        return RecommendationActionEvents.query(
            RecommendationActionEvents.recommendation == recommendation_key,
            RecommendationActionEvents.action == action
        ).fetch()

    def get_recommendation_action_event_for_user(self, recommendation_key, action, user_key):
        return RecommendationActionEvents.query(
            RecommendationActionEvents.user == user_key,
            RecommendationActionEvents.recommendation == recommendation_key,
            RecommendationActionEvents.action == action
        ).get()

    def get_recommendation_events_for_user(self, user, action):
        return RecommendationActionEvents.query(
            RecommendationActionEvents.action == action,
            RecommendationActionEvents.user == user.key
        ).fetch()
