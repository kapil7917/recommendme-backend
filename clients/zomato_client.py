
from clients.base_client import BaseClient
import logging
import json
import urllib3, urllib.parse, re
import requests

class ZomatoClient(BaseClient):

    ZOMATO_BASE_URL = "https://developers.zomato.com/api/v2.1"
    ZOMATO_RESTAURANT_API_PATH = "/restaurant"
    ZOMATO_API_KEY = "5fee2aa0d4f494f5ea15076629dcbbff"

    PHANTOM_JS_API_KEY = "ak-g6qrx-7872k-jmb0c-nq4qv-jqtsf"

    def __init__(self):
        super(BaseClient, self).__init__()

    def get_restaurant_details(self, restaurant_id):
        self._url = self.ZOMATO_BASE_URL + self.ZOMATO_RESTAURANT_API_PATH + "?res_id=" + restaurant_id
        self._headers = {"user-key": self.ZOMATO_API_KEY, "Accept": "application/json"}
        response = self.get()

        if response:
            response = json.loads(response)
            restaurant_details = self._parse_restaurant_details(response)
            return restaurant_details
        return None

    # This parses the restaurant images from zomato website
    def get_restaurant_images(self):
        logging.warning("TEST_PHANTOM1")

        json_obj = {"url": "https://www.zomato.com/bangalore/loft-koramangala-6th-block-bangalore/photos?category=food",
                   "renderType": "script",
                   "outputAsJson": "true"}

        urldict = {}
        urldict['request'] = json_obj

        self._url = "https://PhantomJsCloud.com/api/browser/v2/" + self.PHANTOM_JS_API_KEY + "/?" + urllib.parse.urlencode(urldict)
        self._headers = {"Accept": "application/json"}

        response = self.get()

        logging.warning("TEST_PHANTOM")
        logging.warning(response)

        # headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36'}
        # response = requests.get(
        #     "https://www.zomato.com/bangalore/loft-koramangala-6th-block-bangalore/photos?category=food",
        #     headers=headers)
        # content = response.content
        # logging.debug("ZOMATO_IMAGE_TESTING")
        # logging.debug(content)

    def _parse_restaurant_details(self, response):
        restaurant_details = {
            'name': response.get('name'),
            'zomato_url': response.get('url'),
            'deeplink': response.get('deeplink'),
            'zomato_highlights_list': response.get('highlights'),
            'cuisines_list': response.get('cuisines'),
            'restaurant_location': response.get('location'),
            'has_online_delivery': response.get('has_online_delivery'),
            'average_cost_for_two': response.get('average_cost_for_two'),
            'user_rating': response.get('user_rating'),
            'featured_image': response.get('featured_image'),
            'phone_numbers': response.get('phone_numbers'),
            'user_photos': self._parse_review_images(response)
        }
        return restaurant_details

    def _parse_review_images(self, response):
        photos_list = []
        photos_list_sorted = []
        photo_urls = []
        response_photos = response.get('photos')

        for photo in response_photos:
            photos_list.append({"url": photo.get('photo').get('url'),
                                "foodie_rank": photo.get('photo').get('user').get('foodie_level_num')})

        #if photos_list and len(photos_list) > 0:
        #    photos_list_sorted = sorted(photos_list, key=lambda i: i['foodie_rank'], reverse=True)

        for photo in photos_list:
            photo_urls.append(photo.get('url'))

        return photo_urls
