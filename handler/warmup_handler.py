
from handler.base_request_handler import BaseRequestHandler
from lib import http_response


class WarmupHandler(BaseRequestHandler):

    def _get(self):
        http_response.set_200_response(self.response, json_data={})
