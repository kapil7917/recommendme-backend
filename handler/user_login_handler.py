import json

from handler.base_request_handler import BaseRequestHandler
from lib import http_response
from lib.user import UserClass
from lib.movie_profile import MovieProfileClass
from clients.facebook_client import FacebookClient


class UserLoginHandler(BaseRequestHandler):

    def _post(self):
        access_token = self.request.get('access_token')

        request_json = json.loads(self.request.body)

        # Check if existing user
        email = request_json.get('email')
        first_name = request_json.get('first_name')
        last_name = request_json.get('last_name')
        gender = request_json.get('gender')
        facebook_userid = request_json.get('id')

        if not FacebookClient().is_access_token_valid(access_token, facebook_userid):
            http_response.set_410_response(self.response, "Access token is invalid")
            return

        if not email:
            http_response.set_400_response(self.response, "Email is not present in login request")
            return

        user = None
        if email:
            user = UserClass(email=email).get_user_by_email()

        if user:
            is_new_user = False

            if not user.first_name:
                user = UserClass().set_user_profile_data(userId=user.key.id(),
                                                         first_name=first_name,
                                                         last_name=last_name,
                                                         email=email,
                                                         facebook_userid=facebook_userid,
                                                         gender=gender)

            UserClass().update_access_token(user, access_token)
            
            movie_profile = MovieProfileClass().get_movie_profile(user=user.key)
            if not movie_profile:
                is_new_user = True

            http_response.set_200_response(self.response, {'is_new_user': is_new_user,
                                                           'userid': user.key.id(),
                                                           'user_details': UserClass().get_user_profile_info(userid=user.key.id())})
            return
        else:
            user = UserClass(email=email).create_user(first_name=first_name,
                                                      last_name=last_name,
                                                      gender=gender,
                                                      facebook_userid=facebook_userid,
                                                      access_token=access_token)

            http_response.set_200_response(self.response, {'is_new_user': True,
                                                           'userid': user.key.id(),
                                                           'user_details': UserClass().get_user_profile_info(userid=user.key.id())})
            return
