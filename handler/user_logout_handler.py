
from handler.base_request_handler import BaseRequestHandler
from lib import http_response
from lib.user import UserClass
from clients.facebook_client import FacebookClient


class UserLogoutHandler(BaseRequestHandler):

    def _post(self):
        userid = self.request.get('userid')
        access_token = self.request.get('access_token')

        user = UserClass().get_user_by_id(userid=userid)

        if user:
            if not FacebookClient().is_access_token_valid(access_token, user.facebook_userid):
                http_response.set_410_response(self.response, "Access token is invalid")
                return
        else:
            http_response.set_400_response(self.response, "Invalid user id")
            return

        UserClass().update_access_token(user, access_token=None)
        http_response.set_200_response(self.response, {"message": "Logged out successfully"})
