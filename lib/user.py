from datastore import User, UserFeedback
from lib import constants
from lib.movie_profile import MovieProfileClass
from lib.user_personalisations import UserPersonalisationsClass
from datastore import UserOnDemandMovieRequests
import logging
import datetime


class UserClass(object):

    def __init__(self, email=None):
        self._email = email

    def get_user_by_email(self):
        user = User.query(User.email == self._email).get()
        return user

    def get_user_by_id(self, userid):
        return User.get_by_id(int(userid))

    def create_user(self, first_name, last_name, facebook_userid, gender, access_token):
        user = User(email=self._email,
                    first_name=first_name,
                    last_name=last_name,
                    facebook_userid=facebook_userid,
                    gender=gender,
                    fb_access_token=access_token)
        user.put()
        return user

    def update_access_token(self, user, access_token, firebase_token=None):
        user.fb_access_token = access_token
        user.firebase_token = firebase_token
        user.put()

    def add_user_feedback(self, userid, feedback_text):
        user = self.get_user_by_id(userid)
        feedback = UserFeedback(user=user.key, feedback_text=feedback_text)
        feedback.put()

    @staticmethod
    def get_users(start_cursor=None):
        if start_cursor:
            results, cursor, more = User.query().fetch_page(page_size=100, start_cursor=start_cursor)
        else:
            results, cursor, more = User.query().fetch_page(page_size=100)
        return results, cursor

    def get_user_profile_info(self, userid):
        user = User.get_by_id(userid)
        user_profile = {'first_name': user.first_name,
                        'last_name': user.last_name,
                        'phone_number': user.phone_number,
                        'email': user.email,
                        'interests': [],
                        'personalisations': {},
                        'facebook_userid': user.facebook_userid,
                        'profile_picture': self._get_facebook_profile_pic_url(user.facebook_userid),
                        'movie_profile': MovieProfileClass.get_movie_profile_json(user.key),
                        'on_demand_movies_config': {
                            "max_count": 3,
                            "used_this_month": UserClass().on_demand_movie_requests_count_in_current_month(user_key=user.key),
                            "options": {
                                "date": "Movies for a Date",
                                "everyday": "Nothing Specific, Just share some good movies"
                            }
                        }
                        }
        return user_profile

    def update_user_interest_categories(self, userid, categories):
        user = User.get_by_id(int(userid))
        interests_categories = []

        for category in categories:
            logging.debug(category)
            if category in constants.SUPPORTED_CATEGORIES:
                interests_categories.append(category)

        if len(interests_categories) > 0:
            user.user_selected_interests = interests_categories
        user.put()
        return user.user_selected_interests

    def set_user_profile_data(self, userId=None, first_name=None, last_name=None, email=None, phone_number=None, facebook_userid=None, gender=None):
        user = None
        if userId:
            user = User.get_by_id(int(userId))
        elif email:
            self._email = email
            user = self.get_user_by_email()
        if user:
            logging.debug("Updating profile data for user %s", userId)
            user.first_name = first_name
            user.last_name = last_name
            user.email = email
            user.phone_number = phone_number
            user.facebook_userid = facebook_userid
            user.gender = gender
            user.put()
            return user
        else:
            logging.error("No user found for id %s", userId)

    def _get_facebook_profile_pic_url(self, id):
        if id:
            return "http://graph.facebook.com/" + id + "/picture?type=normal"

    def on_demand_movie_requests_count_in_current_month(self, user_key):
        month_start = datetime.datetime.today().replace(day=1, hour=0, minute=0, second=0, microsecond=0)
        return UserOnDemandMovieRequests.query(UserOnDemandMovieRequests.user==user_key,
                                               UserOnDemandMovieRequests.create_time > month_start).count()
